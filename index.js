// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" let Node.js transfer data using Hyper Text Transfer Protocol aka. HTTP
	// it can create an HTTP server that listens to server ports such as...
	// 3000, 4000, 5000, 8000 (Usually used for web development)
// The "HTTP module" is a set of individual files that contain code to create a "component" that helps establishing data transfer between applications
// Clients (devices/browsers) and server (nodeJS/expressJS application) communicate by exchanging individual messages (request/response)
// Request = the messages sent by the client
// Response = the message set by the server as response



let http = require("http");

// Using the module's createServer() method, we can create an HTTP server that listens to request on a speccified port and gives responses back to the client
// createServer() is a method of the HTTP object responsible for creating a server using Node.js


http.createServer(function (request, response){

	// Use the writeHead method to:
	// set the status code for the response - a 200 means OK
	// Set the content-type of the response - as a plain text message

	response.writeHead(200, {'Content-Type': 'text/plain'})

	// Send the response with the content 'Hello World'
	response.end('Hello World');


}).listen(4000);
// A port is a virtual point where network connections start and end
// The server will be assigned to port 4000 via the listen() method.
	// Where the server will listen to any request that sent to it and will also sent the response via this port

// When server is running, console will print the message:
console.log('Server is running at localhost: 4000');
// The messages or outputs from using console.log will now be displayed to the terminal


